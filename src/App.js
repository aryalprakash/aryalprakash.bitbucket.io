import React, { Component } from 'react';

import {Header} from './Components/Header.js';
import {Main} from './Components/Main.js';

var FontAwesome = require('react-fontawesome');
import ReactSVG from 'react-svg';
import FaHorn from 'react-icons/lib/fa/bullhorn';
import FaQuestion from 'react-icons/lib/fa/question-circle-o';
import FaSettings from 'react-icons/lib/fa/cog';
import FaDown from 'react-icons/lib/fa/caret-down';
import FaDrop from 'react-icons/lib/fa/angle-down';
import FaDots from 'react-icons/lib/fa/ellipsis-v';
import FaBars from 'react-icons/lib/fa/bars';
import Dropdown from 'react-dropdown';
import Popover from 'react-simple-popover';
import {RadialBarChart, RadialBar, Legend} from 'recharts';

export default class App extends Component {
  render() {
    return (<div className="container">
      <Header />
      <Main />
      </div>
    );
  }
}