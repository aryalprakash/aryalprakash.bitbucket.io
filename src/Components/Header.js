import React, { Component } from 'react';
var FontAwesome = require('react-fontawesome');
import ReactSVG from 'react-svg';
import FaHorn from 'react-icons/lib/fa/bullhorn';
import FaQuestion from 'react-icons/lib/fa/question-circle-o';
import FaSettings from 'react-icons/lib/fa/cog';
import FaDown from 'react-icons/lib/fa/caret-down';
import FaBars from 'react-icons/lib/fa/bars';
import Popover from 'react-simple-popover';

export class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false
    };
  }

  handleClick(e) {
    this.setState({open: !this.state.open});
  }

  handleClose(e) {
    this.setState({open: false});
  }

  render(){
    return(
      <div className="navbar">
        <div className="navbar-container">
        <div className="navbar-left">
          <div className="logo">
            <img src={require("../../images/logo.png")} width="60px" />
          </div>
            <div className="navWide">
              <div className="nav-menus">
                  <a href="#" className="menu highlighter">Dashboard <FaDown className="fa-down" /></a>
                  <a href="#" className="menu">Edit Time</a>
                  <a href="#" className="menu">Screenshots</a>
                  <a href="#" className="menu">Reports <FaDown className="fa-down" /></a>
                  <a href="#" className="menu">Payments <FaDown className="fa-down" /></a>
                </div>
            </div>
            <div className="navNarrow" ref="menu">
              <FaBars style={{fontSize: 25, color: '#657fac', cursor: 'pointer'}} onClick={this.handleClick.bind(this)} />
                <div className="narrowLinks">
                    <Popover
                    placement='bottom'
                    style={{padding: 0, borderRadius: 4}}
                    target={this.refs.menu}
                    show={this.state.open}
                    onHide={this.handleClose.bind(this)} >
                    <div className="more-options">
                      <div className="Dropdown-option" onClick={this.handleClick.bind(this)}>Dashboard</div>
                      <div className="Dropdown-option" onClick={this.handleClick.bind(this)}>Edit Time</div>
                      <div className="Dropdown-option" onClick={this.handleClick.bind(this)}>Screenshots</div>
                      <div className="Dropdown-option" onClick={this.handleClick.bind(this)}>Reports</div>
                      <div className="Dropdown-option" onClick={this.handleClick.bind(this)}>Payments</div>
                    </div>
                  </Popover>
                </div>
          </div>
        </div>
        
        <div className="navbar-right">
          <div className="right-menus">
            <div className="menu-icon">
              <FaHorn />
            </div>
            <div className="menu-icon">
              <FaQuestion />
            </div>
            <div className="menu-icon">
              <FaSettings />
              <FaDown className="fa-down" />
            </div>
            <div className="menu-icon menu-profile">
              <img src={require('../../images/avatar.jpg')} className="menu-profile-image"/>
            </div>
          </div>
          <div className="menu-line">
          </div>
          <div className="menu-drop">
            <img src={require('../../images/nav-right.png')} className="menu-drop-image"/>
            <FaDown className="fa-down" />
          </div>
        </div>
        </div>
      </div>
    )
  }
}
