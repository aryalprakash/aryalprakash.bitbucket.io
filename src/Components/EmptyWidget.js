import React, { Component } from 'react';

export class EmptyWidget extends Component{
  render(){
    return(<div className="widget">
      <img src={require('../../images/hours.png')} width="100%" />
    </div>)
  }
}