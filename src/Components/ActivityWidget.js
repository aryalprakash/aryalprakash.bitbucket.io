import React, { Component } from 'react';
import FaDown from 'react-icons/lib/fa/caret-down';
import FaDots from 'react-icons/lib/fa/ellipsis-v';
import Dropdown from 'react-dropdown';
import Popover from 'react-simple-popover';

import {BarChart} from './BarChart.js';

export class ActivityWidget extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false
    };
  }

  handleClick(e) {
    this.setState({open: !this.state.open});
  }

  handleClose(e) {
    this.setState({open: false});
  }

  render(){
    const options = ['Today', 'Last 7 days', 'Last 30 days'];
    let data = {
      background: '#EE352E',
      users: [{
        name: "Raju Mazumder",
        percentage: "13",
        image: null,
        initials: 'RM',
        color: '#EE352E'
      },
      {
        name: "Khurram Butt",
        percentage: "13",
        image: '../../images/khurram.png',
        initials: 'KB',
        color: '#EE352E'
      },
      {
        name: "Eslam Mahmoud",
        percentage: "16",
        image: null,
        initials: 'EM',
        color: '#5A68C6'
      },
      {
        name: "Evgeny Stashevsky",
        percentage: "19",
        image: null,
        initials: 'ES',
        color: '#FFB200'
      },
      {
        name: "Nataliya Oleynyk",
        percentage: "25",
        image: null,
        initials: 'NO',
        color: '#36C398'
      }
    ]
    };

    return(<div className="widget">
      <div className="header">
        <div>Keyboard & Mouse Activity </div>
        <div className="actions">
          <div className="actions"><Dropdown options={options} onChange={this._onSelect} value={options[1]} placeholder="Select an option"/></div>
          <div className="more" ref="target"> <FaDots onClick={this.handleClick.bind(this)} /> </div>
          <Popover
          placement='left'
          style={{padding: 0, borderRadius: 4}}
          target={this.refs.target}
          show={this.state.open}
          onHide={this.handleClose.bind(this)} >
          <div className="more-options">
            <div className="Dropdown-option" onClick={this.handleClick.bind(this)}>Edit Widget</div>
            <div className="Dropdown-option" onClick={this.handleClick.bind(this)}>Delete Widget</div>
          </div>
        </Popover>
        </div>
      </div>
      <div className="content">
        <BarChart {...data} />
      </div>
      <div className="footer">
        <div className="users">
            <div className="user" style={{background: '#ffcd2c'}}>MA</div>
            <div className="user" style={{background: '#4f5dbf', marginLeft: '-8px'}} >DM</div>
            <div className="user" style={{ marginLeft: '-8px'}}>
              <img src={require('../../images/user-small.png')} width="135%"/>
            </div>
            <div style={{color: '#77909D', fontSize: 14, marginLeft: 5}}>+900 more</div>
        </div>
        <div className="tags">
          LOWEST
        </div>
      </div>
    </div>)
  }
}
