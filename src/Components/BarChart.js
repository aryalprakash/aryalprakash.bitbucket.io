import React, { Component } from 'react';

export class BarChart extends Component{
  render(){    
    return(<div className="activity">
      <div className="users">
        {this.props.users.map((user, index) =>
        <div className="user" key={index}>
          <div className="icon" style={{background: user.color}}>
             {user.initials}
          </div>
          <div className="name">
              {user.name}
          </div>
          <div className="line-chart">
              <div className="full-line">
                <div className="fill-line" style={{background:this.props.background, width: `${user.percentage}%`}}></div>
              </div>
          </div>
          <div className="percentage">
            <b>{user.percentage}%</b>
          </div>
        </div>
        )}
        </div>
    </div>)
  }
}
