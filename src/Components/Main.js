import React, { Component } from 'react';
import {ActivityWidget} from './ActivityWidget.js';
import {MobileTimeWidget} from './MobileTimeWidget.js';
import {EmptyWidget} from './EmptyWidget.js';

export class Main extends Component {
  render(){
    return(<div className="main">
      <div className="header">
        <div><h2 className="dashboard-title">Company Dashboard</h2></div>
        <div>
          <div className="button normal">Edit Layout</div>
          <div className="button grey">
            <div className="plus">+</div>
            <div>Add Widget</div>
          </div>
        </div>
      </div>
      <div className="widgets">
        <ActivityWidget />
        <MobileTimeWidget />
        <EmptyWidget />
      </div>
    </div>)
  }
}