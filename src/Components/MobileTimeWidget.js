import React, { Component } from 'react';
import FaDown from 'react-icons/lib/fa/caret-down';
import FaDots from 'react-icons/lib/fa/ellipsis-v';
import Dropdown from 'react-dropdown';
import Popover from 'react-simple-popover';
import {RadialBarChart, RadialBar, Legend} from 'recharts';

export class MobileTimeWidget extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false
    };
  }

  handleClick(e) {
    this.setState({open: !this.state.open});
  }

  handleClose(e) {
    this.setState({open: false});
  }

  render(){
    const options = ['Daily', 'Weekly', 'Monthly'];
    return(<div className="widget">
      <div className="header">
        <div>Highest percentage of Mobile Time Users </div>
        <div className="actions">
          <div><Dropdown options={options} onChange={this._onSelect} value={options[1]} placeholder="Select an option"/></div>
          <div className="more" ref="target"> <FaDots onClick={this.handleClick.bind(this)} /> </div>
          <Popover
          placement='left'
          style={{padding: 0, borderRadius: 4}}
          target={this.refs.target}
          show={this.state.open}
          onHide={this.handleClose.bind(this)} >
          <div className="more-options">
            <div className="Dropdown-option" onClick={this.handleClick.bind(this)}>Edit Widget</div>
            <div className="Dropdown-option" onClick={this.handleClick.bind(this)}>Delete Widget</div>
          </div>
        </Popover>
        </div>
      </div>
      <div className="content radial-chart">
        <MobileTimeChart />
      </div>
      <div className="footer">
        <div className="users">
            <div className="user" style={{background: '#ffcd2c'}}>TY</div>
            <div className="user" style={{background: '#4f5dbf', marginLeft: '-8px'}} >NO</div>
        </div>
        <div className="tags">
          HIGHEST
        </div>
      </div>
    </div>)
  }
}

class MobileTimeChart extends Component {
  render(){

    const data = [
          {name: 'Dmitry Shytsko', mins: 30, pv: '30m', fill: '#f54757'},
          {name: 'Lester Douglas', mins: 85, pv: '1h 25m', fill: '#ffcd2c'},
          {name: 'Elle Javier - Quingco', mins: 560, pv: '9h 20m', fill: '#4f5dbf'}
        ];
        
      const style = {
        top: 0,
        left: 350,
        lineHeight: '24px'
      };

      const renderLegend = (props) => {
      const { payload } = props;

      return (
        <div>
          {
            data.slice(0).reverse().map((user, index) => (
              <div key={`user-${index}`} className="legend-list">
                <div className="circle-back" style={{background: user.fill}}></div>
                <div className="legend-content">
                    <div className="name">{user.name}</div>
                    <b>{user.pv}</b>
                </div>
              </div>
            ))
          }
        </div>
      );
    }

    return <RadialBarChart width={450} clockWise={false} height={215} innerRadius="45%" outerRadius="80%" cx="70%" cy="65%" data={data}>
  <RadialBar startAngle={0} endAngle={15} minAngle={15} clockWise={false}  background  dataKey='mins' />
  <Legend content={renderLegend}  iconSize={10} iconType="circle" width={220} height={140} layout='vertical' verticalAlign='middle' align="left" />
</RadialBarChart>
  }
}