## Dependencies

* react 
* react-icons
* react-dropdown
* react-simple-popover
* recharts

## Run locally

```
Clone this repo and 

1. npm install
2. npm start

```